print("Sorting algos")

def quickSort(start,end,l):
    print(start,end,l)
    if start>=end:
        return    
    pivot ,i ,j = l[start],start+1,end

    while i<=j:

        if pivot >= l[i]:
            i+=1
        elif pivot <= l[j]:
            j-=1
        else:
            temp = l[i]
            l[i]=l[j]
            l[j]=temp
        print("\t\t",l)

    l[start]=l[j]
    l[j]=pivot
    print("\t\tFinal of this ",l)
    quickSort(start,j,l)
    quickSort(j+1,end,l)

def quickSortStart(wholeList):
    quickSort(0,len(wholeList)-1,wholeList)

def insertionSort(wholeList):
    for i in range(len(wholeList)):
        j=i-1
        current = l[i]
        while j>=0:
            if current<l[j]:
                l[j+1]=l[j]
                j-=1
            else:
                break
        l[j+1]=current

def merge(p1,p2):
    result = list()
    i = 0
    j = 0
    while ((i+j)<(len(p1)+len(p2)-1)):
        if p1[i]>p2[j]:
            result.append(p1[i])
            i+=1
        else:
            result.append(p2[j])
            j+=1
    return result


def mergeSort(l):
    if len(l) <= 1:
        return l
    mid = int(len(l)/2)
    p1 = mergeSort(l[:mid])
    p2 = mergeSort(l[mid:])
    return merge(p1,p2)


if __name__ == "__main__":
    l = [3,5,4,1,8,10,9]
    l = mergeSort(l)
    print(l)