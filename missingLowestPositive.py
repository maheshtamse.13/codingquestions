# Good morning! Here's your coding interview problem for today.

# This problem was asked by Stripe.

# Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers as well.

# For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

# You can modify the input array in-place.
#     1 2 3 4 5 6 7 8 9
#   1 2 3 4 5 6 7 8 9

def markList(i,l):
    if i<0 or i>=len(l):
        return
    if l[i] == -1:
        return
    print(i,l)
    element = l[i]
    l[i] = -1
    markList(element-1, l)


def main():
    li = [1]
    li.append(0)
    for i in range(len(li)):
        if li[i] < 0:
            li[i] = 0
    print(li)
    for i in range(len(li)):
        markList(li[i]-1, li)
    print(li)
    for i in range(len(li)):
        if li[i] != -1:
            return i+1
    return len(li)

if __name__ == '__main__':
    print(main())




