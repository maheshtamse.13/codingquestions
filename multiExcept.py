# This problem was asked by Uber.
#
# Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.
#
# For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
#
# Follow-up: what if you can't use division?

# 12345
def multi(left, i, l, r):
    if i == len(l):
        return 1
    m = multi(left*l[i], i+1, l, r)
    print(i,left,m)
    r[i] = left*m
    return l[i]*m


inp = [1, 2, 3, 4, 5]
result = inp.copy()
multi(1, 0, inp, result)
print(result)
