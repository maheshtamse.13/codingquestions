#
# Good morning! Here's your coding interview problem for today.
#
# This problem was asked by Google.
#
# Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s), which deserializes the string back into the tree.
#
# For example, given the following Node class
#
# class Node:
#     def __init__(self, val, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
# The following test should pass:
#
# node = Node('root', Node('left', Node('left.left')), Node('right'))
# assert deserialize(serialize(node)).left.left.val == 'left.left'
#



class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def inOrder(n):
    if n is None:
        return ""
    l = inOrder(n.left)
    r = inOrder(n.right)
    return "{}{}{}".format(l,n.val,r)


def postOrder(n):
    if n is None :
        return ""
    l=postOrder(n.left)
    r=postOrder(n.right)
    return "{}{}{}".format(l,r,n.val)


def serialize(n):
    return inOrder(n)+postOrder(n)


def deserialize(s):
    if not len(s):
        return None
    l = len(s)//2
    inorder = s[:l]
    postorder = s[l:]
    splitIndex = inorder.rindex(postorder[-1])
    left = deserialize(inorder[:splitIndex]+postorder[:splitIndex])
    l = inorder[splitIndex+1:]
    right = deserialize(l+postorder[splitIndex:splitIndex+len(l)])
    root = Node(postorder[-1])
    root.left = left
    root.right = right
    return root


def buildTree(s):
    pass


def main():
    root = Node("h")
    root.left = Node("e")
    root.right = Node("l")
    root.right.right = Node("s")
    root.left.left = Node("l")
    root.left.right = Node("o")
    print(inOrder(root))
    print(postOrder(root))
    s = serialize(root)
    print(s)
    r = deserialize(s)
    if r is None:
        print("None")
    print(inOrder(r))
    print(postOrder(r))


if __name__ == '__main__':
    main()