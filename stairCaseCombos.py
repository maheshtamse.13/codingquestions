# Given a list of numbers, find if there exists a pythagorean triplet in that list. A pythagorean triplet is 3 variables a, b, c where a2 + b2 = c2
#
# Example:
# Input: [3, 5, 12, 5, 13]
# Output: True
# Here, 52 + 122 = 132.
#
# def findPythagoreanTriplets(nums):
#   # Fill this in.
#
# print findPythagoreanTriplets([3, 12, 5, 13])
# # True


# more solution at https://www.geeksforgeeks.org/count-ways-reach-nth-stair/

import math


def staircase(num):
    no2 = num//2
    no1 = num % 2
    n = no1+no2
    r = no1
    ncr = math.factorial(n)//(math.factorial(r)*math.factorial(n-r))
    combinations = ncr
    for i in range(no2):
        newNcr = ((n+1)*(n-r))/((r+2)*(r+1))*ncr
        combinations+=newNcr
        ncr=newNcr
        n+=1
        r+=2
    return combinations
#
#
# print(staircase(4))
# # 5
# print(staircase(5))
# # 8
def calculateRightHalf(i,li,leftMulti):
    if i is len(li):
        return 1
    else:
        right = li[i]*calculateRightHalf(i+1, li, leftMulti*li[i])
        print(leftMulti,i,right)
        return right


l = [1,2,3,4,5]
print(calculateRightHalf(1, l, 1))
#
# # *5*4*3*2*1
# def fact(n):
#     if n is 1:
#         return 1
#     else:
#         return fact(n-1)*n
#
# # 1*2*3*4*5
# #   2 * 3...
# #       6 * ...
# #           24*..
#
# # 1+2+3
# #   3
# #      6
#
#
# def factAgain(i,n,current):
#     if n == i:
#         current*=i
#         print(current)
#     else:
#         factAgain(i+1,n,current*i)
#
# # factAgain(1,5,1)
#
# def factNew(i,n):
#     if n == i:
#         return i
#     else:
#         return i*factNew(i+1,n)
#         # 1 * 2 * 3 * 4 *5
# # factNew(1,5)
